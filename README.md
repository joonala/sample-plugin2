# SamplePlugin2
![](https://github.com/joonala/sample-plugin2/workflows/Tests/badge.svg)
![](https://github.com/joonala/sample-plugin2/workflows/TestsLTR/badge.svg)
[![codecov.io](https://codecov.io/github/joonala/sample-plugin2/coverage.svg?branch=master)](https://codecov.io/github/joonala/sample-plugin2?branch=master)
![](https://github.com/joonala/sample-plugin2/workflows/Release/badge.svg)


### Development

Refer to [development](docs/development.md) for developing this QGIS3 plugin.

## License
This plugin is licenced with
[GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.html).
See [LICENSE](LICENSE) for more information.
